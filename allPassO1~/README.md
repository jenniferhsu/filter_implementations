allPassO1~

desc: Pd external that implements a one pole, one zero all pass filter
currently not working entirely properly --> the phase should be changing with changes to the frequency.  this may be an error in the c code or the Pd example -- I need to figure this out

equations are adapted from Tom Erbe's document on filters


created for Pd-0.45-02

author: jennifer hsu (jsh008@ucsd.edu)
date: october 29, 2013
