// ========================================================================
// name: allPassO1~.c
// desc: Pd external object that implements a one pole all-pass filter
//      left inlet: input signal to filter
//      left inlet with a float: change the cutoff frequency of the filter
//      outlet: filtered signal
// ========================================================================


#include "m_pd.h"
#include <stdlib.h>
#include <math.h>
#ifdef NT
#pragma warning( disable : 4244 )
#pragma warning( disable : 4305 )
#endif

static t_class *allPassO1_class;

typedef struct _allPassO1
{
    t_object x_obj;			// obligatory variable
    t_float x_f;			// stores message recieved on audio inlet (filter cutoff frequency)
    t_float x_c;            // filter coefficient
    t_float sr;             // sample rate
    t_float prev_filt_out;       // last filter output
    t_float prev_filt_in;   // last filter input
} t_allPassO1;

static t_int *allPassO1_perform(t_int *w)
{
    // extract data
	t_allPassO1 *x = (t_allPassO1 *)(w[1]);
    t_float *in = (t_float *)(w[2]);
    t_float *out = (t_float *)(w[3]);
    int blockSize = (int)(w[4]);
    
    t_float insamp;
    
    // update cutoff frequency
    t_float tf = tan(M_PI * x->x_f/x->sr);
    x->x_c = (tf - 1.0f) / (tf + 1.0f);
    
    int samp;
    for( samp = 0; samp < blockSize; samp++ )
    {
        insamp = *(in+samp);
        
        // filter update
        // from LPF: negate the equation
        x->prev_filt_out = (x->x_c * insamp) + x->prev_filt_in - (x->x_c * x->prev_filt_out);
        x->prev_filt_in = insamp;
        
        *(out+samp) = x->prev_filt_out;
    }

    return (w+5);
}

static void allPassO1_dsp(t_allPassO1 *x, t_signal **sp)
{
    x->sr = sp[0]->s_sr;
    dsp_add(allPassO1_perform, 4, x, sp[0]->s_vec, sp[1]->s_vec, sp[0]->s_n);
    
    // reset filter
    x->prev_filt_out = 0.0f;
    x->prev_filt_in = 0.0f;
}

static void *allPassO1_new(t_floatarg f)
{
	t_allPassO1 *x = (t_allPassO1 *)pd_new(allPassO1_class);
    outlet_new(&x->x_obj, gensym("signal"));

    // intialize
	x->x_f = f;
    x->prev_filt_out = 0.0f;
    x->prev_filt_in = 0.0f;
    
    return (x);
}


void allPassO1_tilde_setup(void)
{
    // JENNIFER:
    // notice the third argument is our deallocation function
    allPassO1_class = class_new(gensym("allPassO1~"), (t_newmethod)allPassO1_new, 0,
    	sizeof(t_allPassO1), 0, A_DEFFLOAT, 0);
    CLASS_MAINSIGNALIN(allPassO1_class, t_allPassO1, x_f);
    class_addmethod(allPassO1_class, (t_method)allPassO1_dsp, gensym("dsp"), (t_atomtype)0);
}