onePoleHPF~

desc: Pd external that implements a one pole high pass filter
equation for the filter is a slightly modified version of the low pass filter where we negate the signs for the input and output terms in the filter update. the filter frequency is also specified a bit differently as the frequencies flip, so we have to subtract the frequencies from -0.5 before updating the filter
equations are adapted from Tom Erbe's document on filters

created for Pd-0.45-02

author: jennifer hsu (jsh008@ucsd.edu)
date: october 29, 2013
