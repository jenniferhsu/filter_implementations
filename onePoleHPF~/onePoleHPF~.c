// ========================================================================
// name: onePoleHPF~.c
// desc: Pd external object that implements a one pole filter
//      left inlet: input signal to filter
//      left inlet with a float: change the cutoff frequency of the filter
//      outlet: filtered signal
// ========================================================================


#include "m_pd.h"
#include <stdlib.h>
#include <math.h>
#ifdef NT
#pragma warning( disable : 4244 )
#pragma warning( disable : 4305 )
#endif

static t_class *onePoleHPF_class;

typedef struct _onePoleHPF
{
    t_object x_obj;			// obligatory variable
    t_float x_f;			// stores message recieved on audio inlet (filter cutoff frequency)
    t_float x_c;            // filter coefficient
    t_float sr;             // sample rate
    t_float filt_out;        // filter output
} t_onePoleHPF;

static t_int *onePoleHPF_perform(t_int *w)
{
    // extract data
	t_onePoleHPF *x = (t_onePoleHPF *)(w[1]);
    t_float *in = (t_float *)(w[2]);
    t_float *out = (t_float *)(w[3]);
    int blockSize = (int)(w[4]);
    
    // update cutoff frequency
    // from LPF: negate the spectrum
    x->x_c = ( float )exp(-2.0*M_PI*(0.5f - (x->x_f/x->sr)));
    
    int samp;
    for( samp = 0; samp < blockSize; samp++ )
    {
        // filter update
        // from LPF: negate the equation
        x->filt_out = - *(in + samp) * (1.0f - x->x_c) - x->filt_out * x->x_c;
        
        *(out+samp) = x->filt_out;
    }

    return (w+5);
}

static void onePoleHPF_dsp(t_onePoleHPF *x, t_signal **sp)
{
    x->sr = sp[0]->s_sr;
    dsp_add(onePoleHPF_perform, 4, x, sp[0]->s_vec, sp[1]->s_vec, sp[0]->s_n);
}

static void *onePoleHPF_new(t_floatarg f)
{
	t_onePoleHPF *x = (t_onePoleHPF *)pd_new(onePoleHPF_class);
    outlet_new(&x->x_obj, gensym("signal"));

	x->x_f = f;
    
    return (x);
}


void onePoleHPF_tilde_setup(void)
{
    // JENNIFER:
    // notice the third argument is our deallocation function
    onePoleHPF_class = class_new(gensym("onePoleHPF~"), (t_newmethod)onePoleHPF_new, 0,
    	sizeof(t_onePoleHPF), 0, A_DEFFLOAT, 0);
    CLASS_MAINSIGNALIN(onePoleHPF_class, t_onePoleHPF, x_f);
    class_addmethod(onePoleHPF_class, (t_method)onePoleHPF_dsp, gensym("dsp"), (t_atomtype)0);
}