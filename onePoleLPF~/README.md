onePoleLPF~

desc: Pd external that implements a one pole low pass filter
equation for the filter is an average of the input and output of the filter that was shown to me by Tom Erbe

created for Pd-0.45-02

author: jennifer hsu (jsh008@ucsd.edu)
date: october 29, 2013
